package sample;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.*;
import java.util.*;

public class Main extends Application {

    Stage window;
    BorderPane root;
    Scene scene;

    VBox registerVbox;
    VBox logInVbox;

    ArrayList<User> users = new ArrayList<>();

    TextField userNameTf;
    PasswordField passTf;
    PasswordField confirmPassTf;
    TextField userNameTf2;
    PasswordField passTf2;

    Scanner fin;

    @Override
    public void start(Stage primaryStage) throws Exception{
        window = primaryStage;
        root = new BorderPane();

        // Populate the list using the file
        fin = new Scanner(new File("users.txt"));
        readFromFile();

        /*
                            MENU BAR
        */
        MenuBar menuBar = new MenuBar();
        Menu fileMenu = new Menu("File");
        MenuItem logInItem = new MenuItem("Log In");
        MenuItem registerItem = new MenuItem("Register");
        SeparatorMenuItem line = new SeparatorMenuItem();
        MenuItem exitItem = new MenuItem("Exit");
        fileMenu.getItems().addAll(logInItem,registerItem,line,exitItem);
        menuBar.getMenus().add(fileMenu);

        logInItem.setOnAction(e -> changeSceneToLogIn());
        registerItem.setOnAction(e -> changeSceneToRegister());
        exitItem.setOnAction(e -> exitProgram());

        /*
                            REGISTRATION
        */
        // Labels, button, text(pass)fields for registration
        Label userNameLabel = new Label("Username");
        Label passLabel = new Label("Password");
        Label confirmPassLabel = new Label("Confirm password");

        Button registerButton = new Button("Register");

        userNameTf = new TextField();
        userNameTf.setPromptText("username");
        passTf = new PasswordField();
        passTf.setPromptText("password");
        confirmPassTf = new PasswordField();
        confirmPassTf.setPromptText("password");

        VBox vb1 = new VBox(23,userNameLabel,passLabel,confirmPassLabel);
        VBox vb2 = new VBox(10,userNameTf,passTf,confirmPassTf,registerButton);
        HBox hb1 = new HBox(10,vb1,vb2);
        hb1.setAlignment(Pos.CENTER);

        registerVbox = new VBox(20,hb1);
        registerVbox.setAlignment(Pos.CENTER);

        // Register the user (check if the info is valid and add the user to the file/list)
        registerButton.setOnAction(e -> registerUser());

        /*
                            LOGGING IN
        */
        // Labels, button, text(pass)fields for logging in
        Label userNameLabel2 = new Label("Username");
        Label passLabel2 = new Label("Password");

        userNameTf2 = new TextField();
        userNameTf2.setPromptText("username");
        passTf2 = new PasswordField();
        passTf2.setPromptText("password");

        Button logInButton = new Button("Log In");

        VBox vb3 = new VBox(22,userNameLabel2,passLabel2);
        VBox vb4 = new VBox(10,userNameTf2,passTf2,logInButton);
        HBox hb2 = new HBox(10,vb3,vb4);
        hb2.setAlignment(Pos.CENTER);

        logInVbox = new VBox(20,hb2);
        logInVbox.setAlignment(Pos.CENTER);

        logInButton.setOnAction(e -> logInUser());

        root.setTop(menuBar);
        root.setCenter(logInVbox);

        scene = new Scene(root, 370, 300, Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.setTitle("Register window");
        scene.getStylesheets().add
                (Main.class.getResource("paprika.css").toExternalForm());
        window.show();
    }

    private void readFromFile(){
        String name = "", pass = "";
        User user;

        // Go through the file and collect info about the users
        while(fin.hasNext()){
            name = fin.next();
            pass = fin.next();

            // Create user and add it to the list
            user = new User();
            user.setUserName(name);
            user.setPassword(pass);
            users.add(user);
        }
    }

    private boolean userValidation(String name){
        for (User user : users) {
            if (name.equals(user.getUserName()))
                // we already have an user with this name
                return false;
        }
        return true;
    }

    private void registerUser(){
        if(!passTf.getText().equals(confirmPassTf.getText())) {
            // Show an alert if the confirmPass doesn't match with the pass
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setContentText("Error in confirming the password.");
            alert.show();
            passTf.clear();
            confirmPassTf.clear();
        }else if(passTf.getText().equals("")) {
            // Show an alert if the user doesn't type any password
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setContentText("Please type a password.");
            alert.show();
            passTf.clear();
            confirmPassTf.clear();
        }else if(userNameTf.getText().equals("")){
            // Show an alert if the user doesn't type any username
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setContentText("Please type an username.");
            alert.show();
            passTf.clear();
            confirmPassTf.clear();
        }else if(!userValidation(userNameTf.getText())){
            // Show an alert if the userName is already registered
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setContentText("Username already registered.");
            alert.show();
            userNameTf.clear();
            passTf.clear();
            confirmPassTf.clear();
        }else {
            String name = userNameTf.getText();
            String pass = passTf.getText();

            // Create an user and add it to the list
            User user = new User();
            user.setUserName(name);
            user.setPassword(pass);
            users.add(user);

            // Print the updated list to the file
            try {
                PrintStream output = new PrintStream(new File("users.txt"));
                for (User user1 : users) {
                    output.println(user1.getUserName() + " " + user1.getPassword());
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            // Show an alert to inform the user that the registration was successful
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setContentText("Registration successful !");
            alert.show();
            userNameTf.clear();
            passTf.clear();
            confirmPassTf.clear();
        }

    }

    private void logInUser(){
        int  found = 0;
        for(User user : users) {
            if(user.getUserName().equals(userNameTf2.getText()) &&
                    user.getPassword().equals(passTf2.getText())){
                // User found
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setContentText("Welcome, "+user.getUserName()+"!");
                alert.show();
                userNameTf2.clear();
                found = 1;
                break;
            }else if(user.getUserName().equals(userNameTf2.getText()) &&
                    !user.getPassword().equals(passTf2.getText())){
                // Found user, but incorrect password
                found = 2;
                break;
            }
        }
        if(found == 0){
            // Show an alert to inform that this user doesn't exist
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("User doesn't exist !");
            alert.show();
            userNameTf2.clear();
        }else if(found == 2){
            // Show an alert to inform that the password is incorrect
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("Incorrect password !");
            alert.show();
        }
        passTf2.clear();
    }

    private void changeSceneToLogIn(){
        root.setCenter(logInVbox);
    }

    private void changeSceneToRegister(){
        root.setCenter(registerVbox);
    }

    private void exitProgram(){
        window.close();
    }


    public static void main(String[] args) {
        launch(args);
    }
}